#version 330

uniform float time_seconds;
uniform vec3 material_colour;

in vec3 varying_position;
in vec3 varying_normal;
in vec2 varying_texcoord;

flat in int instance;

layout(location = 0) out vec3 fragment_position;
layout(location = 1) out vec3 fragment_normal;
layout(location = 2) out vec3 fragment_colour;

void main(void)
{
	fragment_position = varying_position;
	fragment_normal = varying_normal;
	fragment_colour = material_colour;
}