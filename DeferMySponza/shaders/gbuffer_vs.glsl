#version 330

uniform float time_seconds;
uniform mat4 projection_view_transform[100];
uniform mat4 model_transform[100];

in vec3 vertex_position;
in vec3 vertex_normal;
in vec2 vertex_texcoord;

out vec3 varying_normal;
out vec2 varying_texcoord;
out vec3 varying_position;

flat out int instance;

void main(void)
{
	varying_texcoord = vertex_texcoord;

	instance = gl_InstanceID;

	varying_position = mat4x3(model_transform[gl_InstanceID]) * vec4(vertex_position, 1.0);

	varying_normal = mat3(model_transform[gl_InstanceID]) * normalize(vertex_normal);

	gl_Position = projection_view_transform[gl_InstanceID] * vec4(vertex_position, 1.0);
}
