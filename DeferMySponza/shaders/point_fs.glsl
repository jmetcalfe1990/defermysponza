#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_colour;

uniform vec3 light_position;
uniform float light_range;
uniform vec3 light_intensity;

out vec3 reflected_light;

void main(void)
{
	vec3 diff_intensity = vec3(0.0, 0.0, 0.0);
	vec3 k_diffuse = vec3(1.0, 1.0, 1.0);

	vec3 texel_N = texelFetch(sampler_world_normal,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 texel_P = texelFetch(sampler_world_position,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 texel_C = texelFetch(sampler_world_colour,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 N = normalize(texel_N);

	vec3 L = normalize(light_position - texel_P);

	float dist = distance(light_position, texel_P);
	float dist_factor_attenuation = 1.0 - smoothstep(light_range * 0.75, light_range, dist);
	vec3 light_at_point = (dist_factor_attenuation * light_intensity);

	float LN = max(0.0, dot(L, normalize(N)));

	diff_intensity += k_diffuse * LN * light_at_point;

	reflected_light = vec3((0.5 + 0.5) * diff_intensity) * texel_C;
}