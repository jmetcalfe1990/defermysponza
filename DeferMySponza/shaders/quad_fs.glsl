#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_colour;

uniform vec3 light_direction;
uniform vec3 light_intensity;

out vec3 reflected_light;

void main(void)
{
	vec3 diff_intensity = vec3(0.0, 0.0, 0.0);

	vec3 texel_N = texelFetch(sampler_world_normal,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 texel_C = texelFetch(sampler_world_colour,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 N = normalize(texel_N);

	vec3 L = normalize(-light_direction);
	float LN = max(0.0, dot(L, N));

	diff_intensity = vec3(0.5 + 0.5) * LN;

	reflected_light = light_intensity * diff_intensity * texel_C;
}