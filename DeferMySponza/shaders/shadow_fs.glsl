#version 330

flat in int instance;

out vec3 shadow_depth;

void main(void)
{
	shadow_depth = vec3(gl_FragCoord.z);
}
