#version 330

uniform float time_seconds;
uniform mat4 depth_projection_view_xform[100];
uniform mat4 model_xform[100];

in vec3 vertex_position;

flat out int instance;

void main(void)
{
	instance = gl_InstanceID;

	gl_Position = depth_projection_view_xform[gl_InstanceID] * model_xform[gl_InstanceID] * vec4(vertex_position, 1.0);
}
