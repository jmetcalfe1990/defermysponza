#version 330

uniform sampler2DRect sampler_world_position;
uniform sampler2DRect sampler_world_normal;
uniform sampler2DRect sampler_world_colour;
uniform sampler2DRect shadow_tex;

uniform vec3 light_position;
uniform vec3 light_direction;
uniform vec3 light_intensity;
uniform float light_range;
uniform float light_cone_angle;

in vec4 varying_shadowcoord;

out vec3 reflected_light;

void main(void)
	{
	vec3 k_diffuse = vec3(1.0, 1.0, 1.0);

	vec3 texel_N = texelFetch(sampler_world_normal,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 texel_P = texelFetch(sampler_world_position,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 texel_C = texelFetch(sampler_world_colour,
		ivec2(gl_FragCoord.xy)).rgb;

	vec3 N = normalize(texel_N);

	vec3 L = normalize(light_position - texel_P);

	vec3 diff_intensity = vec3(0.0, 0.0, 0.0);
	vec3 L_direction = normalize(light_direction);

	float dist = distance(light_position, texel_P);
	float dist_factor_attenuation = 1.0 - smoothstep(0, light_range, dist);
	vec3 light_at_point = (dist_factor_attenuation * light_intensity);

	float cone = smoothstep(cos(light_cone_angle * 0.5), 1, dot(-L, L_direction));

	vec3 shadowcoord = varying_shadowcoord.xyz / varying_shadowcoord.w;
	ivec2 shadowpixel = ivec2(textureSize(shadow_tex) * (shadowcoord.xy * 0.5 + 0.5));

	float shadow_depth_1 = texelFetch(shadow_tex, shadowpixel).r;
	float shadow_depth_2 = texelFetch(shadow_tex, shadowpixel + ivec2(1, 0)).r;
	float shadow_depth_3 = texelFetch(shadow_tex, shadowpixel + ivec2(1, 1)).r;
	float shadow_depth_4 = texelFetch(shadow_tex, shadowpixel + ivec2(0, 1)).r;

	float shadow_result_1 = float((shadowcoord.z * 0.5 + 0.5) <= (shadow_depth_1 + 0.00003));
	float shadow_result_2 = float((shadowcoord.z * 0.5 + 0.5) <= (shadow_depth_2 + 0.00003));
	float shadow_result_3 = float((shadowcoord.z * 0.5 + 0.5) <= (shadow_depth_3 + 0.00003));
	float shadow_result_4 = float((shadowcoord.z * 0.5 + 0.5) <= (shadow_depth_4 + 0.00003));

	float shadow_atten = 0.25 * (shadow_result_1 + shadow_result_2 + shadow_result_3 + shadow_result_4);

	float LN = max(0.0, dot(L, normalize(N)));
	diff_intensity += k_diffuse * LN * light_at_point * shadow_atten;

	reflected_light = cone * vec3((0.5 + 0.5) * diff_intensity) * texel_C * shadow_atten;
	}