#version 330
uniform mat4 projection_view_model_xform;
uniform mat4 model_xform;
uniform mat4 shadow_look;
uniform mat4 inverse_look;
uniform mat4 shadow_projection_view_model_xform;

in vec3 vertex_position;

out vec4 varying_shadowcoord;

void main(void)
	{
	varying_shadowcoord = shadow_projection_view_model_xform * model_xform * vec4(vertex_position, 1.0);
	gl_Position = varying_shadowcoord;
	}
