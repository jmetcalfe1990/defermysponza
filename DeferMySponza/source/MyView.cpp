#include "MyView.hpp"

#include <sponza/sponza.hpp>
#include <tygra/FileHelper.hpp>
#include <tsl/shapes.hpp>

#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <cassert>

MyView::MyView()
	{
	}

MyView::~MyView() {
	}

void MyView::setScene(const sponza::Context * scene)
	{
	scene_ = scene;
	}

void MyView::windowViewWillStart(tygra::Window * window)
	{
	assert(scene_ != nullptr);

#pragma region Geometry.
	sponza::GeometryBuilder builder;

	const auto& scene_meshes = builder.getAllMeshes();
	std::vector<Vertex> vertices;
	std::vector<unsigned int> elems;

	// Constructing scene meshes.
	for (const auto& scene_mesh : scene_meshes)
		{
		MeshGL& new_mesh = meshes_[scene_mesh.getId()];

		const auto& source_mesh = builder.getMeshById(scene_mesh.getId());
		const auto& positions = source_mesh.getPositionArray();
		const auto& elements = source_mesh.getElementArray();
		const auto& normals = source_mesh.getNormalArray();
		const auto& texcoords = source_mesh.getTextureCoordinateArray();
		const auto& instances = scene_->getInstancesByMeshId(scene_mesh.getId());

		// Checking for the number of instances.
		new_mesh.instance_count = instances.size();
		new_mesh.first_vertex_index = vertices.size();

		for (int i = 0; i < source_mesh.getPositionArray().size(); i++)
			{
			Vertex vert;
			vert.position = (const glm::vec3 &)positions[i];
			vert.normal = (const glm::vec3 &)normals[i];

			// Checks to see if there are texture co-ordinates within the instance.
			if (texcoords.size() > 0)
				{
				vert.texCoord = (const glm::vec2 &)texcoords[i];
				}
			vertices.push_back(vert);
			}

		// Setting the elements in the index.
		new_mesh.first_element_index = elems.size();
		elems.insert(elems.end(), elements.begin(), elements.end());
		new_mesh.element_count = elements.size();
		}
#pragma endregion

#pragma region Vertex Buffer Object and Array Object.
	{
	// Generating and binding data relevant to interleaved vertex position VBO.
	glGenBuffers(1, &vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		vertices.size() * sizeof(Vertex),
		vertices.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, kNullID);

	// Generating and binding data relevant to interleaved element VBO.
	glGenBuffers(1, &element_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		elems.size() * sizeof(unsigned int),
		elems.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, kNullID);

	// Generating and binding data relevant to single VAO.
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, element_vbo);

	// Binding buffers for data contained within interleaved VBOs.
	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(kVertexPosition);
	glVertexAttribPointer(kVertexPosition, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), TGL_BUFFER_OFFSET_OF(Vertex, position));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(kVertexNorm);
	glVertexAttribPointer(kVertexNorm, 3, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), TGL_BUFFER_OFFSET_OF(Vertex, normal));

	glBindBuffer(GL_ARRAY_BUFFER, vertex_vbo);
	glEnableVertexAttribArray(kVertexCoord);
	glVertexAttribPointer(kVertexCoord, 2, GL_FLOAT, GL_FALSE,
		sizeof(Vertex), TGL_BUFFER_OFFSET_OF(Vertex, texCoord));

	glBindBuffer(GL_ARRAY_BUFFER, kNullID);
	glBindVertexArray(kNullID);
	}

	// Creating the fullscreen quad for global illumination.
	{
	std::vector<glm::vec2> vertices(4);
	vertices[0] = glm::vec2(-1, -1);
	vertices[1] = glm::vec2(1, -1);
	vertices[2] = glm::vec2(1, 1);
	vertices[3] = glm::vec2(-1, 1);

	// Generating and binding data relevant to light quad.
	glGenBuffers(1, &light_quad_mesh.vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_quad_mesh.vertex_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		vertices.size() * sizeof(glm::vec2), //should this be a glm::vec2? If so would the shader need changing to accommodate this
		vertices.data(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, kNullID);

	glGenVertexArrays(1, &light_quad_mesh.vao);
	glBindVertexArray(light_quad_mesh.vao);
	glBindBuffer(GL_ARRAY_BUFFER, light_quad_mesh.vertex_vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, sizeof(glm::vec2), 0);
	glBindBuffer(GL_ARRAY_BUFFER, kNullID);
	glBindVertexArray(kNullID);
	}

	// Creating a sphere for deferred shading with a point light source.
	{
	tsl::IndexedMeshPtr mesh = tsl::createSpherePtr(1.f, 12);
	mesh = tsl::cloneIndexedMeshAsTriangleListPtr(mesh.get());

	light_sphere_mesh.element_count = mesh->indexCount();

	glGenBuffers(1, &light_sphere_mesh.vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_sphere_mesh.vertex_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		mesh->vertexCount() * sizeof(glm::vec3),
		mesh->positionArray(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &light_sphere_mesh.element_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_sphere_mesh.element_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		mesh->indexCount() * sizeof(unsigned int),
		mesh->indexArray(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &light_sphere_mesh.vao);
	glBindVertexArray(light_sphere_mesh.vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_sphere_mesh.element_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_sphere_mesh.vertex_vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(glm::vec3), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	}

	// Creating a cone for deferred shading with a spot light source.
	{
	tsl::IndexedMeshPtr mesh = tsl::createConePtr(1.f, 1.f, 12);
	mesh = tsl::cloneIndexedMeshAsTriangleListPtr(mesh.get());

	light_cone_mesh.element_count = mesh->indexCount();

	glGenBuffers(1, &light_cone_mesh.vertex_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_cone_mesh.vertex_vbo);
	glBufferData(GL_ARRAY_BUFFER,
		mesh->vertexCount() * sizeof(glm::vec3),
		mesh->positionArray(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);

	glGenBuffers(1, &light_cone_mesh.element_vbo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_cone_mesh.element_vbo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER,
		mesh->indexCount() * sizeof(unsigned int),
		mesh->indexArray(),
		GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glGenVertexArrays(1, &light_cone_mesh.vao);
	glBindVertexArray(light_cone_mesh.vao);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, light_cone_mesh.element_vbo);
	glBindBuffer(GL_ARRAY_BUFFER, light_cone_mesh.vertex_vbo);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE,
		sizeof(glm::vec3), 0);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindVertexArray(0);
	}

#pragma endregion

	glGenTextures(1, &gbuffer_position_tex);
	glGenTextures(1, &gbuffer_normal_tex);
	glGenTextures(1, &gbuffer_colour_tex);
	glGenTextures(1, &lbuffer_colour_tex);
	glGenTextures(1, &sbuffer_depth_tex);
	glGenTextures(1, &sbuffer_colour_tex);

	// Generating frame and render buffer objects.
	glGenFramebuffers(1, &gbuffer_fbo);
	glGenFramebuffers(1, &lbuffer_fbo);
	glGenFramebuffers(1, &sbuffer_fbo);
	glGenRenderbuffers(1, &gbuffer_depth_rbo);

	//glPixelStorei(GL_UNPACK_ALIGNMENT, 1);
	//glPixelStorei(GL_UNPACK_SWAP_BYTES, GL_TRUE);

#pragma region Shader Creation
	// Creating Vertex and Fragment shaders for the G-Buffer.
	{
	GLint compile_status = 0;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///gbuffer_vs.glsl");
	const char *vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1, (const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE)
		{
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string
		= tygra::createStringFromFile("resource:///gbuffer_fs.glsl");
	const char *fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1, (const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE)
		{
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	gbuffer_shader_prog = glCreateProgram();
	glAttachShader(gbuffer_shader_prog, vertex_shader);
	glBindAttribLocation(gbuffer_shader_prog, kVertexPosition, "vertex_position");
	glBindAttribLocation(gbuffer_shader_prog, kVertexNorm, "vertex_normal");
	glBindAttribLocation(gbuffer_shader_prog, kVertexCoord, "vertex_texcoord");
	glDeleteShader(vertex_shader);

	glAttachShader(gbuffer_shader_prog, fragment_shader);
	glBindFragDataLocation(gbuffer_shader_prog, kFragmentPosition, "fragment_position");
	glBindFragDataLocation(gbuffer_shader_prog, kFragmentNormal, "fragment_normal");
	glBindFragDataLocation(gbuffer_shader_prog, kFragmentColour, "fragment_colour");
	glDeleteShader(fragment_shader);
	glLinkProgram(gbuffer_shader_prog);

	GLint link_status = 0;
	glGetProgramiv(gbuffer_shader_prog, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(gbuffer_shader_prog, string_length, NULL, log);
		std::cerr << log << std::endl;
		}
	}

	// Creating Vertex and Fragment shaders for the Directional Lighting.
	{
	GLint compile_status = 0;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///quad_vs.glsl");
	const char *vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1,
		(const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string =
		tygra::createStringFromFile("resource:///quad_fs.glsl");
	const char *fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1,
		(const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	quad_shader_prog = glCreateProgram();
	glAttachShader(quad_shader_prog, vertex_shader);
	glBindAttribLocation(quad_shader_prog, 0, "vertex_position");
	glDeleteShader(vertex_shader);
	glAttachShader(quad_shader_prog, fragment_shader);
	glBindFragDataLocation(quad_shader_prog, 0, "reflected_light");
	glDeleteShader(fragment_shader);
	glLinkProgram(quad_shader_prog);

	GLint link_status = 0;
	glGetProgramiv(quad_shader_prog, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(quad_shader_prog, string_length, NULL, log);
		std::cerr << log << std::endl;
		}
	}

	// Creating Vertex and Fragment shaders for the Point lighting.
	{
	GLint compile_status = 0;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///point_vs.glsl");
	const char *vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1,
		(const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string =
		tygra::createStringFromFile("resource:///point_fs.glsl");
	const char *fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1,
		(const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	point_shader_prog = glCreateProgram();
	glAttachShader(point_shader_prog, vertex_shader);
	glBindAttribLocation(point_shader_prog, 0, "vertex_position");
	glDeleteShader(vertex_shader);
	glAttachShader(point_shader_prog, fragment_shader);
	glBindFragDataLocation(point_shader_prog, 0, "reflected_light");
	glDeleteShader(fragment_shader);
	glLinkProgram(point_shader_prog);

	GLint link_status = 0;
	glGetProgramiv(point_shader_prog, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(point_shader_prog, string_length, NULL, log);
		std::cerr << log << std::endl;
		}
	}

	// Creating Vertex and Fragment shaders for the Spot lighting.
	{
	GLint compile_status = 0;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///spot_vs.glsl");
	const char *vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1,
		(const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string =
		tygra::createStringFromFile("resource:///spot_fs.glsl");
	const char *fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1,
		(const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	spot_shader_prog = glCreateProgram();
	glAttachShader(spot_shader_prog, vertex_shader);
	glBindAttribLocation(spot_shader_prog, 0, "vertex_position");
	glDeleteShader(vertex_shader);
	glAttachShader(spot_shader_prog, fragment_shader);
	glBindFragDataLocation(spot_shader_prog, 0, "reflected_light");
	glDeleteShader(fragment_shader);
	glLinkProgram(spot_shader_prog);

	GLint link_status = 0;
	glGetProgramiv(spot_shader_prog, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(spot_shader_prog, string_length, NULL, log);
		std::cerr << log << std::endl;
		}
	}

	// Creating Vertex shader for the Shadow Mapping.
	{
	GLint compile_status = 0;

	GLuint vertex_shader = glCreateShader(GL_VERTEX_SHADER);
	std::string vertex_shader_string
		= tygra::createStringFromFile("resource:///shadow_vs.glsl");
	const char *vertex_shader_code = vertex_shader_string.c_str();
	glShaderSource(vertex_shader, 1,
		(const GLchar **)&vertex_shader_code, NULL);
	glCompileShader(vertex_shader);
	glGetShaderiv(vertex_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(vertex_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	GLuint fragment_shader = glCreateShader(GL_FRAGMENT_SHADER);
	std::string fragment_shader_string
		= tygra::createStringFromFile("resource:///shadow_fs.glsl");
	const char *fragment_shader_code = fragment_shader_string.c_str();
	glShaderSource(fragment_shader, 1,
		(const GLchar **)&fragment_shader_code, NULL);
	glCompileShader(fragment_shader);
	glGetShaderiv(fragment_shader, GL_COMPILE_STATUS, &compile_status);
	if (compile_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetShaderInfoLog(fragment_shader, string_length, NULL, log);
		std::cerr << log << std::endl;
		}

	shadow_shader_prog = glCreateProgram();
	glAttachShader(shadow_shader_prog, vertex_shader);
	glBindAttribLocation(shadow_shader_prog, 0, "vertex_position");
	glDeleteShader(vertex_shader);
	glAttachShader(shadow_shader_prog, fragment_shader);
	glBindFragDataLocation(shadow_shader_prog, 0, "shadow_depth");
	glDeleteShader(fragment_shader);
	glLinkProgram(shadow_shader_prog);

	GLint link_status = 0;
	glGetProgramiv(shadow_shader_prog, GL_LINK_STATUS, &link_status);
	if (link_status != GL_TRUE) {
		const int string_length = 1024;
		GLchar log[string_length] = "";
		glGetProgramInfoLog(shadow_shader_prog, string_length, NULL, log);
		std::cerr << log << std::endl;
		}
	}
#pragma endregion
	}

void MyView::windowViewDidReset(tygra::Window * window,
	int width,
	int height)
	{
	glViewport(0, 0, width, height);
	// G-Buffer:-
	// Binding gbuffer position and normal textures.
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_colour_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB32F, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// Binding lbuffer colour texture.
	glBindTexture(GL_TEXTURE_RECTANGLE, lbuffer_colour_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_RGB8, width, height, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// Binding the sbuffer colour texture.
	glBindTexture(GL_TEXTURE_RECTANGLE, sbuffer_colour_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_R32F, shadow_width, shadow_height, 0, GL_RGB, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);

	// Binding gbuffer renderbuffer.
	glBindRenderbuffer(GL_RENDERBUFFER, gbuffer_depth_rbo);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH24_STENCIL8, width, height);

	// Binding gbuffer framebuffer.
	GLenum framebuffer_status = 0;
	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);

	// Inserting attachment for depth stencil, position, normal and colour.
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, gbuffer_position_tex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_TEXTURE_RECTANGLE, gbuffer_normal_tex, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_RECTANGLE, gbuffer_colour_tex, 0);

	framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE)
		{
		tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "framebuffer not complete");
		}

	const GLenum buffers[] = { GL_COLOR_ATTACHMENT0, GL_COLOR_ATTACHMENT1, GL_COLOR_ATTACHMENT2 };
	glDrawBuffers(3, buffers);

	// Closing gbuffer framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	// L-Buffer:-
	// Binding the lbuffer framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, lbuffer_fbo);

	// Inserting attachment for depth stencil and colour.
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, lbuffer_colour_tex, 0);

	framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE)
		{
		tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "framebuffer not complete");
		}

	const GLenum lBuffer[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, lBuffer);

	// Closing lbuffer framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, 0);

	//// S-Buffer:-
	// Binding the sbuffer framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, sbuffer_fbo); /////// SHADOW STUFF *********

	glBindTexture(GL_TEXTURE_RECTANGLE, sbuffer_depth_tex);
	glTexImage2D(GL_TEXTURE_RECTANGLE, 0, GL_DEPTH_COMPONENT32, shadow_width, shadow_height, 0, GL_DEPTH_COMPONENT, GL_FLOAT, nullptr);
	glBindTexture(GL_TEXTURE_RECTANGLE, 0);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_RECTANGLE, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	glDrawBuffer(GL_NONE);
	glReadBuffer(GL_NONE);

	//glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_STENCIL_ATTACHMENT, GL_RENDERBUFFER, gbuffer_depth_rbo);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_RECTANGLE, sbuffer_depth_tex, 0); /////// SHADOW STUFF *********
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_RECTANGLE, sbuffer_colour_tex, 0);

	framebuffer_status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
	if (framebuffer_status != GL_FRAMEBUFFER_COMPLETE)
		{
		tglDebugMessage(GL_DEBUG_SEVERITY_HIGH_ARB, "framebuffer not complete"); /////// SHADOW STUFF *********
		}

	const GLenum sBuffer[] = { GL_COLOR_ATTACHMENT0 };
	glDrawBuffers(1, lBuffer);

	// Closing the sbuffer framebuffer.
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	}

void MyView::windowViewDidStop(tygra::Window * window)
	{
	// Deleting the appropriate vertex and map data.
	for (auto& m_mesh : meshes_)
		{
		auto mesh_id = m_mesh.first;
		auto& mesh = m_mesh.second;

		glDeleteBuffers(1, &vertex_vbo);
		glDeleteBuffers(1, &element_vbo);
		glDeleteVertexArrays(1, &vao);
		}

	glDeleteProgram(gbuffer_shader_prog);
	glDeleteProgram(quad_shader_prog);
	glDeleteProgram(point_shader_prog);
	glDeleteProgram(spot_shader_prog);
	glDeleteProgram(shadow_shader_prog);

	glDeleteBuffers(1, &light_quad_mesh.vertex_vbo);
	glDeleteBuffers(1, &light_quad_mesh.element_vbo);
	glDeleteVertexArrays(1, &light_quad_mesh.vao);

	glDeleteBuffers(1, &light_sphere_mesh.vertex_vbo);
	glDeleteBuffers(1, &light_sphere_mesh.element_vbo);
	glDeleteVertexArrays(1, &light_sphere_mesh.vao);

	glDeleteBuffers(1, &light_cone_mesh.vertex_vbo);
	glDeleteBuffers(1, &light_cone_mesh.element_vbo);
	glDeleteVertexArrays(1, &light_cone_mesh.vao);

	glDeleteTextures(1, &gbuffer_position_tex);
	glDeleteTextures(1, &gbuffer_normal_tex);
	glDeleteTextures(1, &gbuffer_colour_tex);
	glDeleteTextures(1, &lbuffer_colour_tex);
	glDeleteTextures(1, &sbuffer_depth_tex);
	glDeleteTextures(1, &sbuffer_colour_tex);

	glDeleteFramebuffers(1, &gbuffer_fbo);
	glDeleteFramebuffers(1, &lbuffer_fbo);
	glDeleteFramebuffers(1, &sbuffer_fbo);
	glDeleteRenderbuffers(1, &gbuffer_depth_rbo);
	}

void MyView::windowViewRender(tygra::Window * window)
	{
	assert(scene_ != nullptr);

	glDepthMask(GL_TRUE);

	glBindFramebuffer(GL_FRAMEBUFFER, gbuffer_fbo);

	glClearColor(0.f, 0.f, 0.25f, 0.f);
	glClear(GL_COLOR_BUFFER_BIT);
	glClear(GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);

	glDepthFunc(GL_LESS);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_CULL_FACE);
	glCullFace(GL_BACK);
	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_ALWAYS, 128, ~0);
	glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);

#pragma region Projection, View, Aspect Ratio.
	GLint viewport_size[4];
	glGetIntegerv(GL_VIEWPORT, viewport_size);
	const float aspect_ratio = viewport_size[2] / (float)viewport_size[3];

	// Add camera to the scene.
	const auto& cam = scene_->getCamera();
	glm::vec3 camera_position = (const glm::vec3 &)cam.getPosition();

	glm::mat4 projection = glm::perspective(glm::radians(cam.getVerticalFieldOfViewInDegrees()), aspect_ratio,
		cam.getNearPlaneDistance(), cam.getFarPlaneDistance());
	glm::mat4 view = glm::lookAt(camera_position, camera_position + (const glm::vec3 &)cam.getDirection(),
		(const glm::vec3 &)scene_->getUpDirection());
#pragma endregion

#pragma region Instance Draw.
	// Geometry Pass.
	glUseProgram(gbuffer_shader_prog);

	glBindVertexArray(vao);

	for (const auto& mesh : meshes_)
		{
		const auto& instances = scene_->getInstancesByMeshId(mesh.first);

		for (int i = 0; i < instances.size(); i++)
			{
			auto transform_string = "projection_view_transform[" + std::to_string(i) + "]";
			auto model_string = "model_transform[" + std::to_string(i) + "]";

			const auto& instance_ID = scene_->getInstanceById(instances[i]);
			const auto& material_ID = scene_->getMaterialById(instance_ID.getMaterialId());

			glm::mat4 model = (const glm::mat4x3 &)(instance_ID.getTransformationMatrix());
			glm::mat4 projection_view_transform = projection * view * model;

			glm::vec3 material_colour = (const glm::vec3 &)material_ID.getDiffuseColour();

			glUniformMatrix4fv(glGetUniformLocation(gbuffer_shader_prog, model_string.c_str()), 1, GL_FALSE, glm::value_ptr(model));
			glUniformMatrix4fv(glGetUniformLocation(gbuffer_shader_prog, transform_string.c_str()), 1, GL_FALSE, glm::value_ptr(projection_view_transform));

			glUniform3fv(glGetUniformLocation(gbuffer_shader_prog, "material_colour"), 1, glm::value_ptr(material_colour));
			}

		glDrawElementsInstancedBaseVertex(GL_TRIANGLES, mesh.second.element_count, GL_UNSIGNED_INT,
			(void*)(mesh.second.first_element_index * sizeof(int)), mesh.second.instance_count, mesh.second.first_vertex_index);
		}

	// Directional/Ambient Pass.
	glUseProgram(quad_shader_prog);

	glBindFramebuffer(GL_READ_FRAMEBUFFER, gbuffer_fbo);

	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, lbuffer_fbo);

	glClear(GL_COLOR_BUFFER_BIT);

	glEnable(GL_STENCIL_TEST);
	glStencilFunc(GL_NOTEQUAL, 0, ~0);
	glStencilOp(GL_KEEP, GL_KEEP, GL_KEEP);

	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex);
	glUniform1i(glGetUniformLocation(quad_shader_prog, "sampler_world_position"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex);
	glUniform1i(glGetUniformLocation(quad_shader_prog, "sampler_world_normal"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_colour_tex);
	glUniform1i(glGetUniformLocation(quad_shader_prog, "sampler_world_colour"), 2);

	glBindVertexArray(light_quad_mesh.vao);

	for (const auto& d_lights : scene_->getAllDirectionalLights())
		{
		glUniform3fv(glGetUniformLocation(quad_shader_prog, "light_direction"), 1, glm::value_ptr((const glm::vec3 &)d_lights.getDirection()));
		glUniform3fv(glGetUniformLocation(quad_shader_prog, "light_intensity"), 1, glm::value_ptr((const glm::vec3 &)d_lights.getIntensity()));

		glDrawArrays(GL_TRIANGLE_FAN, 0, 4);
		}

	glDepthMask(GL_FALSE);

	// Additive blending.
	glEnable(GL_BLEND);
	glBlendEquation(GL_FUNC_ADD);
	glBlendFunc(GL_ONE, GL_ONE);

	glUseProgram(point_shader_prog);

	glBindVertexArray(light_sphere_mesh.vao);

	glDepthFunc(GL_GREATER);
	glCullFace(GL_FRONT);

	// Binding textures to the fragment shader for gbuffer data.
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex);
	glUniform1i(glGetUniformLocation(point_shader_prog, "sampler_world_position"), 0);

	glActiveTexture(GL_TEXTURE1);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex);
	glUniform1i(glGetUniformLocation(point_shader_prog, "sampler_world_normal"), 1);

	glActiveTexture(GL_TEXTURE2);
	glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_colour_tex);
	glUniform1i(glGetUniformLocation(point_shader_prog, "sampler_world_colour"), 2);


	for (const auto& p_lights : scene_->getAllPointLights())
		{
		// Build a model matrix using light positions and projection/view matrices.
		glm::mat4 model_xform = glm::translate(glm::mat4(1), (const glm::vec3 &)p_lights.getPosition()) *
			glm::scale(glm::mat4(1), glm::vec3(p_lights.getRange()));

		glm::mat4 projection_view_xform = projection * view * model_xform;

		glUniformMatrix4fv(glGetUniformLocation(point_shader_prog, "projection_view_xform"), 1, GL_FALSE, glm::value_ptr(projection_view_xform));
		glUniformMatrix4fv(glGetUniformLocation(point_shader_prog, "model_xform"), 1, GL_FALSE, glm::value_ptr(model_xform));

		glUniform3fv(glGetUniformLocation(point_shader_prog, "light_position"), 1, glm::value_ptr((const glm::vec3 &)p_lights.getPosition()));
		glUniform3fv(glGetUniformLocation(point_shader_prog, "light_intensity"), 1, glm::value_ptr((const glm::vec3 &)p_lights.getIntensity()));
		glUniform1f(glGetUniformLocation(point_shader_prog, "light_range"), p_lights.getRange());

		// Drawing the elements.
		glDrawElements(GL_TRIANGLES, light_sphere_mesh.element_count, GL_UNSIGNED_INT, 0);
		}

	for (const auto& s_lights : scene_->getAllSpotLights())
		{
		// Unbinding texture.
		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_RECTANGLE, 0);

		// Binding Shadow frame buffer.
		glBindFramebuffer(GL_FRAMEBUFFER, sbuffer_fbo);

		// glViewport setup for size to match shadow frame buffer resolution.
		glViewport(0, 0, shadow_width, shadow_height);

		// Pipeline configuration.
		glDepthMask(GL_TRUE);
		glClear(GL_DEPTH_BUFFER_BIT);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);
		glDepthFunc(GL_LEQUAL);
		glDisable(GL_STENCIL_TEST);
		glDisable(GL_BLEND);
	
		// Use shadow shader program.
		glUseProgram(shadow_shader_prog);

		glBindVertexArray(vao);
	 
		for (const auto& mesh : meshes_)
			{
			const auto& instances = scene_->getInstancesByMeshId(mesh.first);

			glm::mat4 depth_projection_matrix = glm::perspective(glm::radians(s_lights.getConeAngleDegrees()), 1.0f, 0.1f, s_lights.getRange());

			glm::mat4 depth_view_matrix = glm::lookAt((const glm::vec3 &)s_lights.getPosition(),
				(const glm::vec3 &)s_lights.getPosition() + (const glm::vec3 &)s_lights.getDirection(),
				(const glm::vec3 &)scene_->getUpDirection());

			for (int i = 0; i < instances.size(); i++)
				{
				auto transform_string = "depth_projection_view_xform[" + std::to_string(i) + "]";
				auto model_string = "model_xform[" + std::to_string(i) + "]";

				const auto& instance_ID = scene_->getInstanceById(instances[i]);

				glm::mat4 model = (const glm::mat4x3 &)(instance_ID.getTransformationMatrix());

				glm::mat4 depth_projection_view_xform = depth_projection_matrix * depth_view_matrix;

				glUniformMatrix4fv(glGetUniformLocation(shadow_shader_prog, model_string.c_str()), 1, GL_FALSE, glm::value_ptr(model));
				glUniformMatrix4fv(glGetUniformLocation(shadow_shader_prog, transform_string.c_str()), 1, GL_FALSE, glm::value_ptr(depth_projection_view_xform));
				}

			glDrawElementsInstancedBaseVertex(GL_TRIANGLES, mesh.second.element_count, GL_UNSIGNED_INT,
				(void*)(mesh.second.first_element_index * sizeof(int)), mesh.second.instance_count, mesh.second.first_vertex_index);
			}

		glViewport(0, 0, viewport_size[2], viewport_size[3]);

		glBindFramebuffer(GL_FRAMEBUFFER, lbuffer_fbo);

		glDepthMask(GL_FALSE);

		glEnable(GL_BLEND);
		glBlendEquation(GL_FUNC_ADD);
		glBlendFunc(GL_ONE, GL_ONE);

		glDepthFunc(GL_GREATER);
		glEnable(GL_CULL_FACE);
		glCullFace(GL_FRONT);

		glUseProgram(spot_shader_prog);

		glBindVertexArray(light_cone_mesh.vao);

		// Binding textures to the fragment shader for gbuffer data.
		glActiveTexture(GL_TEXTURE0);
		glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_position_tex);
		glUniform1i(glGetUniformLocation(spot_shader_prog, "sampler_world_position"), 0);

		glActiveTexture(GL_TEXTURE1);
		glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_normal_tex);
		glUniform1i(glGetUniformLocation(spot_shader_prog, "sampler_world_normal"), 1);

		glActiveTexture(GL_TEXTURE2);
		glBindTexture(GL_TEXTURE_RECTANGLE, gbuffer_colour_tex);
		glUniform1i(glGetUniformLocation(spot_shader_prog, "sampler_world_colour"), 2);

		glActiveTexture(GL_TEXTURE3);
		glBindTexture(GL_TEXTURE_RECTANGLE, sbuffer_colour_tex);
		glUniform1i(glGetUniformLocation(spot_shader_prog, "shadow_tex"), 3);

		// Build a model matrix using light positions and projection/view matrices.
		float cone = s_lights.getRange() * glm::tan(glm::radians(s_lights.getConeAngleDegrees() * 0.5f));
		glm::mat4 model_xform = glm::scale(glm::mat4(1), glm::vec3(cone, cone, s_lights.getRange())) *
			glm::translate(glm::mat4(1), glm::vec3(0, 0, -1));

		glm::mat4 shadow_projection = glm::perspective(glm::radians(s_lights.getConeAngleDegrees()), 1.f, 0.1f, s_lights.getRange());

		glm::mat4 shadow_look = glm::lookAt((const glm::vec3 &)s_lights.getPosition(),
			(const glm::vec3 &)s_lights.getPosition() + (const glm::vec3 &)s_lights.getDirection(),
			(const glm::vec3 &)scene_->getUpDirection());

		glm::mat4 inverse_look = glm::lookAt((const glm::vec3 &)s_lights.getPosition(),
			(const glm::vec3 &)s_lights.getPosition() + (const glm::vec3 &)s_lights.getDirection(),
			(const glm::vec3 &)scene_->getUpDirection());

		shadow_look = glm::inverse(shadow_look);
;
		glm::mat4 shadow_projection_view_model_xform = shadow_projection * shadow_look;

		glUniformMatrix4fv(glGetUniformLocation(spot_shader_prog, "model_xform"), 1, GL_FALSE, glm::value_ptr(model_xform));
		glUniformMatrix4fv(glGetUniformLocation(spot_shader_prog, "shadow_look"), 1, GL_FALSE, glm::value_ptr(shadow_look));

		glUniformMatrix4fv(glGetUniformLocation(spot_shader_prog, "shadow_projection_view_model_xform"), 1, GL_FALSE, glm::value_ptr(shadow_projection_view_model_xform));

		glUniform3fv(glGetUniformLocation(spot_shader_prog, "light_position"), 1, glm::value_ptr((const glm::vec3 &)s_lights.getPosition()));
		glUniform3fv(glGetUniformLocation(spot_shader_prog, "light_direction"), 1, glm::value_ptr((const glm::vec3 &)s_lights.getDirection()));
		glUniform3fv(glGetUniformLocation(spot_shader_prog, "light_intensity"), 1, glm::value_ptr((const glm::vec3 &)s_lights.getIntensity()));
		glUniform1f(glGetUniformLocation(spot_shader_prog, "light_range"), s_lights.getRange());
		glUniform1f(glGetUniformLocation(spot_shader_prog, "light_cone_angle"), glm::radians(s_lights.getConeAngleDegrees()));

		glDrawElements(GL_TRIANGLES, light_cone_mesh.element_count, GL_UNSIGNED_INT, 0);
		}

	glDepthMask(GL_TRUE);
	glDepthFunc(GL_LEQUAL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_CULL_FACE);
	glDisable(GL_BLEND);

	// Blitting offscreen framebuffer to the screen.
	glBindFramebuffer(GL_READ_FRAMEBUFFER, lbuffer_fbo);
	glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
	glBlitFramebuffer(0, 0, viewport_size[2], viewport_size[3],
		0, 0, viewport_size[2], viewport_size[3], GL_COLOR_BUFFER_BIT, GL_NEAREST);
#pragma endregion
	}
