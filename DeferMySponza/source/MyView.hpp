#pragma once

#include <sponza/sponza_fwd.hpp>
#include <tygra/WindowViewDelegate.hpp>
#include <tgl/tgl.h>
#include <glm/glm.hpp>

#include <vector>
#include <memory>
#include <unordered_map>

class MyView : public tygra::WindowViewDelegate
{
public:

	MyView();

	~MyView();

	void setScene(const sponza::Context * scene);

private:

	void windowViewWillStart(tygra::Window * window) override;

	void windowViewDidReset(tygra::Window * window,
		int width,
		int height) override;

	void windowViewDidStop(tygra::Window * window) override;

	void windowViewRender(tygra::Window * window) override;

	const sponza::Context * scene_;

	// Shader program creation.
	GLuint gbuffer_shader_prog{ 0 };
	GLuint quad_shader_prog{ 0 };
	GLuint point_shader_prog{ 0 };
	GLuint spot_shader_prog{ 0 };
	GLuint shadow_shader_prog{ 0 };

	// GBuffer Framebuffer Object.
	GLuint gbuffer_fbo{ 0 };
	GLuint lbuffer_fbo{ 0 };
	GLuint sbuffer_fbo{ 0 };

	// GBuffer Renderbuffer Object.
	GLuint gbuffer_depth_rbo{ 0 };

	// Gbuffer Textures.
	GLuint gbuffer_position_tex{ 0 };
	GLuint gbuffer_normal_tex{ 0 };
	GLuint gbuffer_colour_tex{ 0 };

	// LBuffer Textures.
	GLuint lbuffer_colour_tex{ 0 };

	// SBuffer Textures.
	GLuint sbuffer_depth_tex{ 0 };
	GLuint sbuffer_colour_tex{ 0 };

	// Interleaved Vertex Buffer Objects.
	GLuint vertex_vbo{ 0 };
	GLuint element_vbo{ 0 };
	GLuint vao{ 0 };

	// Data to generate Vertex Buffer Objects.
	struct Vertex
	{
		glm::vec3 position;
		glm::vec3 normal;
		glm::vec2 texCoord;
	};

	// Data to generate one Vertex Array Object.
	struct MeshGL
	{
		GLuint first_element_index;
		GLuint first_vertex_index;
		GLuint element_count;
		GLuint instance_count;
	};

	// Looping over a map, 'First' element of a map, 'Second' element of a map.
	std::unordered_map<sponza::MeshId, MeshGL> meshes_;

	// Data to generate meshes.
	struct Mesh
	{
		GLuint vertex_vbo{ 0 };
		GLuint element_vbo{ 0 };
		GLuint vao{ 0 };
		int element_count{ 0 };
	};

	Mesh light_quad_mesh;
	Mesh light_sphere_mesh;
	Mesh light_cone_mesh;

	const static GLuint kNullID = 0;
	const unsigned int shadow_width = 2048;
	const unsigned int shadow_height = 2048;

	enum VertexAttributeIndexes
	{
		kVertexPosition = 0,
		kVertexNorm = 1,
		kVertexCoord = 2
	};

	enum FragmentDataIndexes
	{
		kFragmentPosition = 0,
		kFragmentNormal = 1,
		kFragmentColour = 2
	};
};
